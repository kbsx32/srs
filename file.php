<?php
/**
 * Created by PhpStorm.
 * User: vpotseluyko
 * Date: 10.02.17
 * Time: 10:29
 */


define('FILE_NAME', 'test');
@session_start();
if (!isset($_SESSION['auth']) || $_SESSION['auth'] === false){
    @session_write_close();
    die();
}
$id = $_SESSION['id'];
if (!is_dir(__DIR__."/{$id}_files"))
    mkdir(__DIR__."/{$id}_files")   ;
if (!is_dir(__DIR__."/{$id}_files"))
    die();
if (!file_exists(__DIR__."/{$id}_files/meta.txt"))
    file_put_contents(__DIR__."/{$id}_files/meta.txt", json_encode([]));
$files = json_decode(
        file_get_contents(__DIR__."/{$id}_files/meta.txt")
);
$chars = 'abdefhiknrstyzABDEFGHKNQRSTYZ23456789';
$numChars = strlen($chars);
$string = '';
for ($i = 0; $i < 8; $i++)
    $string .= substr($chars, rand(1, $numChars) - 1, 1);
$path_parts = pathinfo($_FILES[FILE_NAME]["name"]);
/// TODO: ext above
$ext = $path_parts['extension'];
$target = __DIR__."/{$id}_files/{$string}.{$ext}";
$files[] = [
    $string.'.'.$ext => $_FILES[FILE_NAME]['name']
];
if (move_uploaded_file($_FILES[FILE_NAME]["tmp_name"], $target)) {
    echo json_encode([
            'name' => $_FILES[FILE_NAME]['name'],
            'url' => "/{$id}_files/{$string}.{$ext}"
    ]);
    file_put_contents(__DIR__."/{$id}_files/meta.txt", json_encode($files));
} else {
    ;
}
@session_write_close();

?>