<?php
/**
 * Created by PhpStorm.
 * User: vpotseluyko
 * Date: 28.01.17
 * Time: 7:59
 */


class Template {

    public $template;

    function __construct()
    {
        $a = func_get_args();
        $i = func_num_args();
        if (method_exists($this, $f = '__construct'.$i)) {
            call_user_func_array(array($this, $f), $a);
        }
    }

    function __construct3 (string $authed, string $nonauthed, bool $auth) {
        $this->template = ($auth) ? $authed : $nonauthed;
    }

    function __construct1(string $template) {
        $this->template = $template;
    }
}
