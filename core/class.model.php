<?php
/**
 * Created by PhpStorm.
 * User: killer
 * Date: 15/09/16
 * Time: 20:38
 */

namespace MVC_light;

class Model extends Ab_Model {

    protected $user;

    protected $template;

    protected $db;

    function __construct(MVC_Core $MVC_core) {
        $this->user = new User;
        $this->MVC_core = $MVC_core;

        $this->db = $this->MVC_core->needs_database();

        if (isset($_SESSION['id']) && $this->db !== null)
            $_SESSION['common_company_id'] = $this->db->get_global_company_id($_SESSION['id']);
    }


    protected function get_common() : array {
        Service::needs('css', 'styles', true);
        Service::needs('css', 'defst', false);
        Service::needs('js', 'AF-light', false);
        if (CSRF_SAFE)
            $this->common['token'] = Service::generate_token();
        $this->common['version'] = VERSION;
        $this->common['site_name'] = SITE_NAME;
        $this->common['company_not_chosen'] = (isset($_SESSION['comp_id']))
                    ? 0 : 1;
        // var_dump($_SESSION['comp_id']);
        if (!isset($_SESSION['comp_id'])) {
            $this->common['companies'] = $this->db->get_companies($_SESSION['id']);
        }
        if (isset($_SESSION['id']) && $this->db !== null) {
            $db = $this->db->get_data($_SESSION['id']);
            $_SESSION['common_company_id'] = $this->db->get_global_company_id($_SESSION['id']);
            $this->common = array_merge($db, $this->common);
            //var_dump($db);
        }
        return $this->common;
    }

    protected function get_js_css() : array {
        return [
            'includes' => self::$dependencies
        ];
    }

    function get_data(string $next_method) : array {
        $specific_params = $this->$next_method();
        $common_params = $this->get_common();
        $includes = $this->get_js_css();
        return array_merge($common_params, $specific_params, $includes);
    }

}