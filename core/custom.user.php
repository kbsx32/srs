<?php
/**
 * Created by PhpStorm.
 * User: vpotseluyko
 * Date: 28.01.17
 * Time: 7:50
 */

namespace MVC_light;


class User
{
    private $authorized;
    private $id;

    /**
     * @return bool
     */
    public function getAuthorized() : bool
    {
        return $this->authorized;
    }

    private function check_auth() {
        if (key_exists('auth', $_SESSION) && $_SESSION['auth'] === true)
            return true;
        return false;
    }

    public function setAuth(bool $auth, $id) {
        $this->authorized = $auth;
        $_SESSION['auth'] = $auth;
        $_SESSION['id'] = $id;
    }

    function __construct() {
        $this->authorized = $this->check_auth();
        if ($this->authorized)
            $this->id = $_SESSION['id'];
    }
}
