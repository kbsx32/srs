<?php
/**
 * Created by PhpStorm.
 * User: killer
 * Date: 18/09/16
 * Time: 11:52
 */

namespace MVC_light;
use mysqli, mysqli_result, mysqli_sql_exception, Exception, Error;


class Mysql_DB extends Ab_Database
{


    protected function query(string $query)
    {
        try {
            return $this->link->query($query);
        } catch (Error $e) {
            Service::error($e);
        }
    }

    protected function get_insert_id()  {
        return $this->link->insert_id;
    }

    protected function query_params(string $query, array $params)
    {
        $i = 0;
        $a = 0;
        foreach ($params as $param)
            $i++;
        $params = array_reverse($params);
        foreach ($params as $param)
            $query = str_replace('$'.($i - $a++), "'".$this->link->real_escape_string($param)."'", $query);
        return $this->query($query);
    }

    protected function fetch_all($resource)
    {
        try {
            return $resource->fetch_all();
        } catch (Error $e) {
            Service::error($e);
        }
    }

    protected function fetch_assoc($resource)
    {
        try {
            return $resource->fetch_assoc();
        } catch (Error $e) {
            Service::error($e);
        }
    }

    protected function close()
    {
        try {
            $this->link->close();
        } catch (Error $e) {
            Service::error($e);
        }
    }

    protected function connect(string $host, string $user, string $password, string $db)
    {
        if (DEBUG)
            mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
        try {
            @$this->link = new mysqli($host, $user, $password, $db);
        } catch (Exception $e) {
            Service::error($e, 'fatal');
        }
        $this->link->set_charset('utf8_general_ci');
        $this->query("SET NAMES utf8");
    }

    public function get_data($id) {
        return $this->fetch_assoc(
            $this->query_params(
                'select * from users where id=$1', [$id]
            )
        );
    }

    /**
     * @php.net http://php.net/manual/ru/mysqli-result.fetch-assoc.php
     */
    protected function fetch_all_assoc($result) {
        for ($set = array (); $row = $result->fetch_assoc(); $set[] = $row);
        return $set;
    }


    public function get_global_company_id($id) {
        return $this->fetch_assoc(
            $this->query_params('select id from org_user WHERE id=$1 LIMIT 1', [$id])
        );
    }

    public function get_companies($id) {
        $common_data = $this->fetch_all_assoc(
            $this->query_params(
                'select * from org_user where user_id=$1', [$id]
            )
        );
        foreach ($common_data as &$case) {
            switch ($case['type']) {
                case 'ur':
                    $case['data'] = $this->fetch_assoc(
                        $this->query_params(
                            'select * from ur_com where id=$1',
                            [$case['type_id']]
                        )
                    );
                    break;
                case 'fiz':
                    $case['data'] = $this->fetch_assoc(
                        $this->query_params(
                            'select * from fix_com where id=$1',
                            [$case['type_id']]
                        )
                    );
                    break;
                case 'ip';
                    $case['data'] = $this->fetch_assoc(
                        $this->query_params(
                            'select * from ip_com where id=$1',
                            [$case['type_id']]
                        )
                    );
                    break;
            }
        }
        //var_dump($common_data);
        //die();
        return $common_data;
    }
}

