<?php


namespace MVC_light\Model;
use \MVC_light\Model as Model,
    \MVC_light\Service as Service;

class Model_desktop extends Model {

    function get_desktop() {

        $template = 'login';
        $events = null;
        if (isset($_SESSION['id'])) {
            $events = $this->db->get_events($_SESSION['comp_id']);
            $template = ($events == null) ? 'main' : 'desktop';
        }
        return [
            'template' => new \Template($template, 'login', $this->user->getAuthorized()),
            'events' => $events
        ];

    }

}