<?php


namespace MVC_light\Model;
use \MVC_light\Model as Model,
    \MVC_light\Service as Service;

class Model_team extends Model {

    function get_team() {

        return [
            'template' => new \Template('team', 'login', $this->user->getAuthorized())
        ];

    }

}