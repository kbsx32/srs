<?php


namespace MVC_light\Model;
use \MVC_light\Model as Model,
    \MVC_light\Service as Service;

class Model_login extends Model {

    function get_login() {

        return [
            'template' => new \Template('login'),
            'login' => true
        ];

    }

}