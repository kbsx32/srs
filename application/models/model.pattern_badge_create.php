<?php


namespace MVC_light\Model;
use \MVC_light\Model as Model,
    \MVC_light\Service as Service;

class Model_pattern_badge_create extends Model {

    function get_pattern_badge_create() {

        return [
            'template' => new \Template('pattern_badge_create', 'login', $this->user->getAuthorized())
        ];

    }

}
