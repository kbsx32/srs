<?php


namespace MVC_light\Model;
use \MVC_light\Model as Model,
    \MVC_light\Service as Service;

class Model_contract_example extends Model {

    function get_contract_example() {

        return [
            'template' => new \Template('contract_example', 'login', $this->user->getAuthorized())
        ];

    }

}
