<?php


namespace MVC_light\Model;
use \MVC_light\Model as Model,
    \MVC_light\Service as Service;

class Model_events_list extends Model {

    function get_events_list() {

        $template = 'login';
        $events = null;
        if (isset($_SESSION['id'])) {
            $events = $this->db->get_events($_SESSION['id']);
            $template = ($events == null) ? 'main' : 'events_list';
        }
        return [
            'template' => new \Template($template, 'login', $this->user->getAuthorized()),
            'events' => $events
        ];
    }
}
