<?php
/**
 * Created by PhpStorm.
 * User: killer
 * Date: 21/08/16
 * Time: 12:25
 */


namespace MVC_light\Model;
use \MVC_light\Model as Model,
    \MVC_light\Service as Service;


class Model_Ajax extends Model {

    function get_ajax() {
        return [

        ];
    }

    function get_login($login, $pass) {
        $data = $this->db->get_user_by_login($login);
        if ($data == NULL)
            return false;
        else if(md5($pass) == $data['password']) {
            if ($data['aemail'] == '1' && $data['asms'] == '1') {
                $this->user->setAuth(true, $data['id']);
                return true;
            }
        }
        return false;
    }

    function end_reg($type, $user_id, $company_id) {
        return $this->db->end_reg($type, $user_id, $company_id);
    }

    function get_events() {
        return $this->db->get_events($_SESSION['id']);
    }

    function create_new_room($theme, $m, $to) {
        $latest_room = $this->db->get_latest_room();
        $latest_room++;
        $this->db->create_new_room($theme, $m, $to, $_SESSION['id'], $latest_room);
        return $latest_room;
    }

    function new_message($room, $to, $m) {
        $this->db->new_message($room, $m, $to, $_SESSION['id']);
    }

    function org_login($login, $pass) {
        if ($this->get_login($login, $pass) === false)
            return false;
        $comp_id = $this->db->check_companies($_SESSION['id']);

        if ($comp_id == NULL)
            return "nocompanies";
        return true;
    }

    function update_bal($n) {
        $user = $this->db->get_data($_SESSION['id']);
        $user['rur'] += $n;
        $this->db->update_rur($user['rur'], $_SESSION['id']);
    }

    function register($company, $site, $phone, $address, $descr, $domain,
                        $inn, $kpp, $ogrn, $bank_name, $bank_account, $bik, $cor_account) {

        $result = $this->db->reg_company($_SESSION['id'], $company, $site, $phone, $address, $descr,
            $domain, $inn, $kpp, $ogrn, $bank_name, $bank_account, $bik, $cor_account);
        $_SESSION['comp_id'] = $result;
        $_SESSION['type'] = "ur_com";
        return ($result == null) ? false : $result;
    }

    function register_ip($company, $ur_address, $f_address, $p_address,
                      $inn, $ogrnip, $bank_name, $bank_account, $bik, $cor_account) {

        $result = $this->db->reg_ip($_SESSION['id'], $company, $ur_address, $f_address, $p_address,
            $inn, $ogrnip, $bank_name, $bank_account, $bik, $cor_account);
        $_SESSION['comp_id'] = $result;
        $_SESSION['type'] = "ip_com";
        return ($result == null) ? false : $result;
    }

    function delete($id) {
        $this->db->delete($id);
    }


    function register_fiz ($fio, $f_address, $p_address,
                      $inn, $bank_name, $bank_account, $bik, $cor_account) {

        $result = $this->db->reg_fiz($_SESSION['id'], $fio, $f_address, $p_address,
            $inn, $bank_name, $bank_account, $bik, $cor_account);
        $_SESSION['comp_id'] = $result;
        $_SESSION['type'] = "fiz_com";
        return ($result == null) ? false : $result;
    }


    function create_event(
        $name, $url, $reg, $confirm, $sdate, $stime, $edate, $etime,
        $shor, $full, $address, $sphere, $tags, $creg, $oreg, $brone, $bronetype,
        $tickets
    ) {
        return $this->db->create_event(
            $name, $url, $reg, $confirm, $sdate, $stime, $edate, $etime,
            $shor, $full, $address, $sphere, $tags, $creg, $oreg, $brone,
            $bronetype, $_SESSION['id'], $_SESSION['comp_id'], $tickets
        );
    }


    function __construct(\MVC_light\MVC_Core $MVC_core) {
        parent::__construct($MVC_core);
        $this->db = $this->MVC_core->needs_database();
    }

}