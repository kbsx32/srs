<?php


namespace MVC_light\Model;
use \MVC_light\Model as Model,
    \MVC_light\Service as Service;

class Model_register_second extends Model {

    function get_register_second() {

        return [
            'template' => new \Template('register_second')
        ];

    }

}