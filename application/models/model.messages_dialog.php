<?php


namespace MVC_light\Model;
use \MVC_light\Model as Model,
    \MVC_light\Service as Service;

class Model_messages_dialog extends Model {

    function get_messages_dialog() {
        $room = $this->db->get_room($_GET['id']);
        $to = ($room[0]['to_id'] == $_SESSION['id']) ? $room[0]['from_id'] : $room[0]['to_id'];
        //var_dump($this->db->get_room($_GET['id']));
        return [
            'template' => new \Template('messages_dialog', 'login', $this->user->getAuthorized()),
            'messages' => $room,
            'id' => $_SESSION['id'],
            'to' => $this->db->get_data($to),
            'from' => $this->db->get_data($_SESSION['id'])
        ];

    }

}
