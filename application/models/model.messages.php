<?php


namespace MVC_light\Model;
use \MVC_light\Model as Model,
    \MVC_light\Service as Service;

class Model_messages extends Model {

    function get_messages() {
        return [
            'template' => new \Template('messages', 'login', $this->user->getAuthorized()),
            'messages' => $this->db->get_messages($_SESSION['id'])
        ];

    }

}
