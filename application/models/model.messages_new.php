<?php


namespace MVC_light\Model;
use \MVC_light\Model as Model,
    \MVC_light\Service as Service;

class Model_messages_new extends Model {

    function get_messages_new() {

        return [
            'users' => $this->db->get_emails(),
            'template' => new \Template('messages_new', 'login', $this->user->getAuthorized())
        ];

    }

}
