<?php


namespace MVC_light\Model;
use \MVC_light\Model as Model,
    \MVC_light\Service as Service;

class Model_pattern_anket extends Model {

    function get_pattern_anket() {

        return [
            'template' => new \Template('pattern_anket', 'login', $this->user->getAuthorized())
        ];

    }

}
