<?php


namespace MVC_light\Model;
use \MVC_light\Model as Model,
    \MVC_light\Service as Service;

class Model_finance_history extends Model {

    function get_finance_history() {

        return [
            'template' => new \Template('finance_history', 'login', $this->user->getAuthorized())
        ];

    }

}
