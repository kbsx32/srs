<?php


namespace MVC_light\Model;
use \MVC_light\Model as Model,
    \MVC_light\Service as Service;

class Model_requisites extends Model {

    function get_requisites() {
        return [
            'template' => new \Template('requisites', 'login', $this->user->getAuthorized())
        ];

    }

}