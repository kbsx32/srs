<?php


namespace MVC_light\Model;
use \MVC_light\Model as Model,
    \MVC_light\Service as Service;

class Model_tariffplan extends Model {

    function get_tariffplan() {

        return [
            'template' => new \Template('tariffplan', 'login', $this->user->getAuthorized())
        ];

    }

}