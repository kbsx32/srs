<?php


namespace MVC_light\Model;
use \MVC_light\Model as Model,
    \MVC_light\Service as Service;

class Model_role extends Model {

    function get_role() {

        return [
            'template' => new \Template('role', 'login', $this->user->getAuthorized())
        ];

    }

}
