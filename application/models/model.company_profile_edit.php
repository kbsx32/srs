<?php


namespace MVC_light\Model;
use \MVC_light\Model as Model,
    \MVC_light\Service as Service;

class Model_company_profile_edit extends Model {

    function get_company_profile_edit() {
        //var_dump($_SESSION['comp_id']);
        return [
            'template' => new \Template('company_profile_edit', 'login', $this->user->getAuthorized())
        ];

    }

}
