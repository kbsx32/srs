<?php


namespace MVC_light\Model;
use \MVC_light\Model as Model,
    \MVC_light\Service as Service;

class Model_company_profile extends Model {

    function get_company_profile() {

        return [
            'template' => new \Template('company_profile', 'login', $this->user->getAuthorized())
        ];

    }

}
