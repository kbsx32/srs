<?php


namespace MVC_light\Model;
use \MVC_light\Model as Model,
    \MVC_light\Service as Service;

class Model_event_stats extends Model {

    function get_event_stats() {
        return [
            'template' => new \Template('event_stats', 'login', $this->user->getAuthorized())
        ];

    }

}