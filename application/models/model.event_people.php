<?php


namespace MVC_light\Model;
use \MVC_light\Model as Model,
    \MVC_light\Service as Service;

class Model_event_people extends Model {

    function get_event_people() {
        return [
            'template' => new \Template('event_people', 'login', $this->user->getAuthorized())
        ];

    }

}