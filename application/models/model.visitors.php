<?php


namespace MVC_light\Model;
use \MVC_light\Model as Model,
    \MVC_light\Service as Service;

class Model_visitors extends Model {

    function get_visitors() {
        $events = $this->db->get_events($_SESSION['comp_id']);
        $id = (isset($_GET['id'])) ? $_GET['id'] : $events[0]['id'];
        $users = $this->db->get_users($id);
        if ($users == null) {
            $users = '0';
        }
        return [
            'template' => new \Template('visitors', 'login', $this->user->getAuthorized()),
            'events' => $events,
            'users' => $users,
            'id' => $id
        ];

    }

}