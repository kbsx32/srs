<?php


namespace MVC_light\Model;
use \MVC_light\Model as Model,
    \MVC_light\Service as Service;

class Model_event_create extends Model {

    function get_event_create() {
        $data = null;
        $test = null;
        if (isset($_GET['id'])) {
            $data = $this->db->get_event($_GET['id']);
            $test = '1';
        }
        //var_dump($data);
        return [
            'template' => new \Template('event_create', 'login', $this->user->getAuthorized()),
            'names' => $this->db->get_names(),
            'data' => $data,
            'edit' => $test
        ];

    }

}
