<?php


namespace MVC_light\Model;
use \MVC_light\Model as Model,
    \MVC_light\Service as Service;

class Model_team_people extends Model {

    function get_team_people() {

        return [
            'users' => $this->db->get_emails(),
            'template' => new \Template('team_people', 'login', $this->user->getAuthorized())
        ];

    }

}