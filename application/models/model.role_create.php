<?php


namespace MVC_light\Model;
use \MVC_light\Model as Model,
    \MVC_light\Service as Service;

class Model_role_create extends Model {

    function get_role_create() {

        return [
            'template' => new \Template('role_create', 'login', $this->user->getAuthorized())
        ];

    }

}