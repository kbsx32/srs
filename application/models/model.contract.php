<?php


namespace MVC_light\Model;
use \MVC_light\Model as Model,
    \MVC_light\Service as Service;

class Model_contract extends Model {

    function get_contract() {

        return [
            'template' => new \Template('contract', 'login', $this->user->getAuthorized())
        ];

    }

}
