<?php


namespace MVC_light\Model;
use \MVC_light\Model as Model,
    \MVC_light\Service as Service;

class Model_register extends Model {

    function get_register() {

        return [
            'template' => new \Template('register')
        ];

    }

}