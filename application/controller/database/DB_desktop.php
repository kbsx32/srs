<?php
/**
 * Created by PhpStorm.
 * User: vpotseluyko
 * Date: 06.02.17
 * Time: 13:04
 */

namespace MVC_light;


class DB_desktop extends Mysql_DB {

    function get_events($id) {
        return $this->fetch_all_assoc(
            $this->query_params('select * from event_new where company_id=$1', [$id])
        );
    }

}