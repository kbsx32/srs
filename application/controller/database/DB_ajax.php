<?php
/**
 * Created by PhpStorm.
 * User: vpotseluyko
 * Date: 28.01.17
 * Time: 19:42
 */

namespace MVC_light;


class DB_ajax extends Mysql_DB
{

    function get_events($id) {
        return $this->fetch_all_assoc(
            $this->query_params(
                'select id, startdate from event_new where user_id=$1', [$id]
            )
        );
    }

    function get_user_by_login(string $login) {
        return $this->fetch_assoc(
            $this->query_params('select * from users where user=$1', [$login])
        );
    }

    function reg_company($id, $company, $site, $phone, $address, $descr,
                         $domain, $inn, $kpp, $ogrn, $bank_name, $bank_account, $bik, $cor_account) {
        $this->query_params(
            "insert into ur_com(user_id, company, site, phone, address, c_descr, domain, inn, ".
            "kpp, ogrn, bank_name, bank_account, bik, cor_account) values ($1, $2, $3, $4, $5, $6, $7, $8, $9, ".
            "$10, $11, $12, $13, $14)", [$id, $company, $site, $phone, $address, $descr,
            $domain, $inn, $kpp, $ogrn, $bank_name, $bank_account, $bik, $cor_account]);
        return $this->get_insert_id();
    }

    function reg_ip($id, $company, $ur_address, $f_address, $p_address,
                    $inn, $ogrnip, $bank_name, $bank_account, $bik, $cor_account) {
        $this->query_params(
            "insert into ip_com(user_id, company, ur_address, f_address, p_address, inn, ".
            "ogrnip, bank_name, bank_account, bik, cor_account) values ($1, $2, $3, $4, $5, $6, $7, $8, $9, ".
            "$10, $11)", [$id, $company, $ur_address, $f_address, $p_address,
            $inn, $ogrnip, $bank_name, $bank_account, $bik, $cor_account]);
        return $this->get_insert_id();
    }
    function reg_fiz($id, $fio, $f_address, $p_address,
                     $inn, $bank_name, $bank_account, $bik, $cor_account) {
        $this->query_params(
            "insert into fiz_com(user_id, company, f_address, p_address, inn, ".
            "bank_name, bank_account, bik, cor_account) values ($1, $2, $3, $4, $5, $6, $7, $8, $9)",
            [$id, $fio, $f_address, $p_address, $inn, $bank_name,
                $bank_account, $bik, $cor_account]);
        return $this->get_insert_id();
    }

    function check_companies($id) {
        return $this->fetch_assoc(
            $this->query_params(
                'select * from org_user where user_id=$1', [$id]
            )
        );
    }

    function get_latest_room() {
        $rooms = $this->fetch_all_assoc(
            $this->query('select * from mail')
        );
        $id = 0;
        foreach ($rooms as $room)
            $id = ($room['room_id'] > $id) ? $room['room_id'] : $id;
        return $id;
    }

    function new_message($room, $m, $to, $from) {
        $this->query_params(
            'insert into mail(room_id, to_id, from_id, text) values ($1, $2, $3, $4)',
            [$room, $to, $from, $m]
        );
    }

    function create_new_room($theme, $m, $to, $from, $room) {
        $this->query_params(
            'insert into mail(to_id, from_id, room_id, title, text) values ($1, $2, $3, $4, $5)', [$to, $from, $room, $theme, $m]
        );
        return $this->get_insert_id();
    }

    function update_rur($n, $id) {
        $this->query_params(
            'update users set rur=$1 where id=$2', [$n, $id]
        );
    }

    function end_reg($type, $user_id, $id) {
        $this->query_params(
            "insert into org_user(type, user_id, type_id) values($1, $2, $3)",
            [$type, $user_id, $id]
        );
        return $this->get_insert_id();
    }

    function delete($id) {
        $this->query_params(
            'delete from event_new where id=$1', [$id]);
    }
    function create_event(
        $name, $url, $reg, $confirm, $sdate, $stime, $edate, $etime,
        $shor, $full, $address, $sphere, $tags, $creg, $oreg, $brone,
        $bronetype, $uid, $cid, $tickets
    ) {
        $this->query_params(
            'insert into event_new(name, url, regmethod, confirm, startdate, '.
            'starttime, enddate, edtime, short, full, address, sphere, '.
            'tags, close_reg, open_closed, event_brone, brone_type, user_id, '.
            'company_id, tickets_json) values ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, '.
            '$11, $12, $13, $14, $15, $16, $17, $18, $19, $20 )', [
                $name, $url, $reg, $confirm, $sdate, $stime, $edate, $etime,
                $shor, $full, $address, $sphere, $tags, $creg, $oreg, $brone,
                $bronetype, $uid, $cid, $tickets]
        );
    }
}