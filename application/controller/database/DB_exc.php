<?php
/**
 * Created by PhpStorm.
 * User: vpotseluyko
 * Date: 07.02.17
 * Time: 23:34
 */

namespace MVC_light;


class DB_exc extends Mysql_DB
{
    function get_names() {
        return $this->fetch_all(
            $this->query_params(
                'select * from events_name', []
            )
        );
    }

    function get_event($id) {
        return $this->fetch_assoc(
            $this->query_params(
                'select * from event_new where id=$1', [$id]
            )
        );
    }
    function get_events($id) {
        return $this->fetch_all_assoc(
            $this->query_params(
                'select * from event_new where company_id=$1', [$id]
            )
        );
    }

    function get_room($id) {
        return $this->fetch_all_assoc(
            $this->query_params(
                'select * from mail where room_id=$1 ORDER BY time', [$id]
            )
        );
    }

    function get_messages($id) {
        $to = $this->fetch_all_assoc( $this->query_params(
            'select room_id from mail where to_id=$1', [$id])
        );
        $from = $this->fetch_all_assoc( $this->query_params(
            'select room_id from mail where from_id=$1', [$id])
        );
        $i = 0;
        $rooms = [];
        $data = [];
        foreach ($to as $room)
            $rooms[$i++] = $room['room_id'];
        foreach ($from as $room)
            $rooms[$i++] = $room['room_id'];
        $rooms = array_unique($rooms);
        $i = 0;
        foreach ($rooms as $room) {
            $data[$i++] = $this->get_room($room);
            $first_message = $data[$i - 1][0];
            if ($first_message['to_id'] == $_SESSION['id'])
                $to = $first_message['from_id'];
            else
                $to = $first_message['to_id'];
            $opponent = $this->get_data($to);
            $data[$i - 1][0]['email'] = $opponent['user'];
            $data[$i - 1][0]['fio'] = $opponent['fio_name'].' '.$opponent['fio_family'];
            //var_dump($opponent['user']);
        }
        //var_dump($data);
        //die();
        return $data;
    }

    function get_users($id) {
        return $this->fetch_all_assoc(
            $this->query_params(
                'select * from event_users where event_id=$1', [$id]
            )
        );
    }

    function get_emails() {
        return $this->fetch_all_assoc(
            $this->query(
                'select id, user from users'
            )
        );
    }
    function get_message($id) {
        return $this->fetch_assoc(
            $this->query_params(
                'select * from mail where id=$1', [$id]
            )
        );
    }
}