<?php
/**
 * Created by PhpStorm.
 * User: vpotseluyko
 * Date: 08.02.17
 * Time: 10:11
 */

namespace MVC_light;


class DB_events_list extends Mysql_DB
{

    function get_events($id) {
        return $this->fetch_all_assoc(
            $this->query_params('select * from event_new where user_id=$1', [$id])
        );
    }
}