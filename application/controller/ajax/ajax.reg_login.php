<?php


namespace MVC_light;

class Ajax_reg_login extends Ajax {

    function action() {
        $this->message['auth'] = $this->controller->model->org_login(
            $this->params['login'], $this->params['pass']
        );
        $this->message['state'] = 'success';
        $this->code = 200;
    }

}