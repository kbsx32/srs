<?php


namespace MVC_light;

class Ajax_register extends Ajax {

    function action() {

        switch ($this->params['type']) {
            case 'ip':
                $comp_id = $this->controller->model->register_ip(
                    $this->params['company'], $this->params['ur-address'],
                    $this->params['fiz-address'], $this->params['post-address'],
                    $this->params['inn'], $this->params['ogrnip'],
                    $this->params['bank_name'], $this->params['bank_account'],
                    $this->params['bik'], $this->params['cor_account']
                );
                $id = $this->controller->model->end_reg('ip', $_SESSION['id'], $comp_id);
                break;
            case 'fiz':
                $comp_id = $this->controller->model->register_fiz(
                    $this->params['fio'], $this->params['fact-address'],
                    $this->params['post-address'],
                    $this->params['inn'],
                    $this->params['bank_name'], $this->params['bank_account'],
                    $this->params['bik'], $this->params['cor_account']
                );
                $id = $this->controller->model->end_reg('fiz', $_SESSION['id'], $comp_id);
                break;
            case 'ur':
                $comp_id = $this->controller->model->register(
                    $this->params['company'], $this->params['site'],
                    $this->params['phone'], $this->params['address'],
                    $this->params['c_descr'], $this->params['domain'],
                    $this->params['inn'], $this->params['kpp'],
                    $this->params['ogrn'], $this->params['bank_name'],
                    $this->params['bank_account'], $this->params['bik'],
                    $this->params['cor_account']
                );
                $id = $this->controller->model->end_reg('ur', $_SESSION['id'], $comp_id);
                break;
            default:
                $this->message['result'] = 'error';
        };
        if (isset($id))
            $_SESSION['common_company_id'] = $id;

        $this->message['state'] = 'success';
        $this->code = 200;
    }

}