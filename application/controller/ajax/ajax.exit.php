<?php


namespace MVC_light;

class Ajax_exit extends Ajax {

    function action() {
        $_SESSION['auth'] = false;
        $this->message['state'] = 'success';
        $this->code = 200;
        header('Location: /');
    }


    function __destruct()
    {
        // TODO: Implement __destruct() method.
    }
}