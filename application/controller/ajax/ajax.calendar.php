<?php


namespace MVC_light;

class Ajax_calendar extends Ajax {

function my_calendar( $fill = [], $events ) {
    $month_names = [
        "январь","февраль","март","апрель","май","июнь",
        "июль","август","сентябрь","октябрь","ноябрь","декабрь"
    ];
    if (isset($_GET['y']))
        $y = $_GET['y'];
    if (isset($_GET['m']))
        $m = $_GET['m'];
    if (isset($_GET['date']) && strstr($_GET['date'], "-"))
        list($y, $m) = explode("-", $_GET['date']);
    if (!isset($y) || $y < 1970 || $y > 2037)
        $y = date("Y");
    if (!isset($m) || $m < 1 || $m > 12)
        $m = date("m");
    $month_stamp = mktime(0,0,0,$m,1,$y);
    $day_count = date("t", $month_stamp);
    $weekday = date("w", $month_stamp);
    if ($weekday == 0)
        $weekday = 7;
    $start = -($weekday - 2);
    $last = ($day_count + $weekday - 1) % 7;
    if ($last == 0)
        $end = $day_count;
    else
        $end = $day_count + 7 - $last;
    $today = date("Y-m-d");
    $prev = date('?\m=m&\y=Y', mktime(0, 0, 0, $m - 1, 1, $y));
    $next = date('?\m=m&\y=Y', mktime(0, 0, 0, $m + 1, 1, $y));
    $i = 0;
    ?>
    <table border=1 cellspacing=0 cellpadding=2>
        <tr>
    <!--        <td colspan=7>
                <table width="100%" border=0 cellspacing=0 cellpadding=0>
                    <tr>
                     --> <td align="left"><a href="<?php echo $prev ?>">&lt;&lt;&lt;</a></td>
                        <td colspan="5" align="center"><?php echo $month_names[$m-1]," ",$y ?></td>
                      <td align="right"><a href="<?php echo $next ?>">&gt;&gt;&gt;</a></td>
                 <!--   </tr>
        <!--        </table>
            </td>
        --></tr>
        <tr><td>Пн</td><td>Вт</td><td>Ср</td><td>Чт</td><td>Пт</td><td>Сб</td><td>Вс</td><tr>
            <?php
            for($d=$start;$d<=$end;$d++) {
                if (!($i++ % 7)) echo " <tr>\n";
                echo '  <td align="center">';
                if ($d < 1 OR $d > $day_count) {
                    echo "&nbsp";
                } else {
                    $now="$y-$m-".sprintf("%02d",$d);
                    $done = false;
                    foreach ($events as $event) {
                        if (!$done)
                            $date = $d;
                        //var_dump($y.'-'.$m.'-'.$d);
                        //var_dump($event['y'].'-'.$event['m'].'-'.$event['d']);
                        //var_dump($d == $event['d'] && $m == $event['m'] &&
                          //  $event['y'] == $y);
                         if (!$done && $d == $event['d'] && $m == $event['m'] &&
                            $event['y'] == $y) {
                            $date = "<a href='/event_create?id={$event['id']}'>$d</a>";
                            $done = true;
                        }
                    }
                    if ((is_array($fill) AND in_array($now, $fill)) || ($d == date("d") && $m == date("m"))) {
                        echo '<b><span class="today">'.$date.'</span></b>';
                    } else {
                        echo $date;
                    }
                }
                echo "</td>\n";
                if (!($i % 7))  echo " </tr>\n";
            }
            ?>
    </table>
<?php }

    function action() {
        $events = $this->controller->model->get_events();
        foreach ($events as &$event) {
            $date = explode('-', $event['startdate']);
            if ($date[0] != '') {
                if (substr($date[2], 0, 1) == '0')
                    $date[2] = substr($date[2], 1, 1);
                $event['m'] = $date[1];
                $event['y'] = $date[0];
                $event['d'] = $date[2];
            } else {
                $event['m'] = $event['y'] = $event['d'] = -1;
            }
        }
        $this->my_calendar([], $events);
        $this->message['state'] = 'success';
        $this->code = 200;
    }

    function __destruct()
    {

    }

}