<?php


namespace MVC_light;

class Ajax_new_event extends Ajax {

    function action() {
        $tickets = $this->params['tickets-json'];
        $name = $this->params['event-name'];
        $url = $this->params['event-url'];
        $reg = $this->params['event-regmethod'];
        $confirm = $this->params['event-confirm'];
        $sdate = $this->params['event-startdate'];
        $stime = $this->params['event-starttime'];
        $edate = $this->params['event-enddata'];
        $etime = $this->params['event-endtime'];
        $shor = $this->params['event-short'];
        $full = $this->params['event-full'];
        $address = $this->params['event-address'];
        $sphere = $this-> params['event-sphere'];
        $tags = $this->params['event-tags'];
        $creg = $this->params['close-reg'];
        $oreg = $this->params['open-closed'];
        $brone = $this->params['event-brone'];
        $bronetype = $this->params['brone-type'];
        $id = $this->controller->model->create_event(
            $name, $url, $reg, $confirm, $sdate, $stime, $edate, $etime,
            $shor, $full, $address, $sphere, $tags, $creg, $oreg, $brone, $bronetype, $tickets
        );
        $this->message['state'] = 'success';
        $this->code = 200;
    }

}