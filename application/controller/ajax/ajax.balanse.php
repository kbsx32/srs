<?php


namespace MVC_light;

class Ajax_balanse extends Ajax {

    function action() {
        $addon = $this->params['summ'];
        $this->controller->model->update_bal($addon);
        $this->message['state'] = 'success';
        $this->code = 200;
    }

}