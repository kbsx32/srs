<?php


namespace MVC_light;

class Ajax_new_room extends Ajax {

    function action() {
        $theme = $this->params['theme'];
        $m = $this->params['message'];
        $to = $this->params['event'];
        $this->message['room'] = $this->controller->model->create_new_room($theme, $m, $to);
        $this->message['state'] = 'success';
        $this->code = 200;
    }

}