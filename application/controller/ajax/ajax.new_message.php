<?php


namespace MVC_light;

class Ajax_new_message extends Ajax {

    function action() {
        $room = $this->params['room_id'];
        $to = $this->params['to_id'];
        $m = $this->params['message'];
        $this->controller->model->new_message($room, $to, $m);
        $this->message['state'] = 'success';
        $this->code = 200;
    }

}