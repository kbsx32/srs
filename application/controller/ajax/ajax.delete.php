<?php


namespace MVC_light;

class Ajax_delete extends Ajax {

    function action() {
        $event_id = $this->params['id'];
        $this->controller->model->delete($event_id);

        $this->message['state'] = 'success';
        $this->code = 200;
    }

}