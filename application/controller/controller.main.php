<?php
/**
 * Created by PhpStorm.
 * User: vpotseluyko
 * Date: 06.02.17
 * Time: 19:03
 */

namespace MVC_light\Controller;


use MVC_light\MVC_Core;

class Controller_main extends \MVC_light\Controller {

    function __construct(MVC_Core $core)
    {
        header("Location: /desktop");
    }

}